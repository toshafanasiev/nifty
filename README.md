# Nifty

A general utility library for .NET development. Contains types representing the following concepts

- Loops
- Null values
- Optional values
- Wrapped actions

## Loops

The `Loop` class enables the packaging up of some common control structures as objects; it was inspired by Python's generators. 

Here are some examples

### For

You can package up a standard `for` loop using the `Loop.For` methods.

Here the `For` method is used to generate the alphabet using a definition rather than an enumeration.

    var alphabet = Loop.For('a', c => c <= 'z', c => ++c);

    var list = alphabet.ToList();

    Assert.AreEqual(26, list.Count);
    var upperAlpha = new String(alphabet.Select(c => Char.ToUpper(c)).ToArray());
    Assert.AreEqual("ABCDEFGHIJKLMNOPQRSTUVWXYZ", upperAlpha);

Note the use of the prefix increment `++c` - if you postfix you'll run into trouble. See this [blog post](http://blog.tosh.me/2012/07/an-ode-to-python-in-c.html) for more detail.

An alternative is to use the following overload.

### For (ref)

This example shows the `For` overload that takes an instance of the `Loop.Advance` delegate.

	var alphabet = Loop.For('a', c => c <= 'z', (ref char c) => c++);

	var list = alphabet.ToList();

	Assert.AreEqual(26, list.Count);
	var upperAlpha = new String(alphabet.Select(c => Char.ToUpper(c)).ToArray());
	Assert.AreEqual("ABCDEFGHIJKLMNOPQRSTUVWXYZ", upperAlpha);

That delegate looks like this

    public delegate void Advance<T>(ref T value);

### While - Yield

To package up a standard `while` loop use the `While - Yield` combination.

    var random = new Random();
    var numbers = Loop.While(run => run.Count < 10)
                        .Yield(run => random.NextDouble())
                        .ToList();

    Assert.AreEqual(numbers.Count, 10);

Here the `LoopRunContext` called `run` is used to track the number of iterations. By closing around the environment you can use anything to track your progress.

    var list = new List<double>();
    var random = new Random();
    var numbers = Loop.While(run => list.Count < 10)
                        .Yield(run => random.NextDouble());
    
    list.AddRange(numbers);

    Assert.AreEqual(list.Count, 10);

To force the evaluation of the enumerable, use `Do`.

    var list = new List<int>();
    Loop.While(run => list.Count < 10)
        .Do(run => list.Add(run.Count));

    Assert.AreEqual(list.Count, 10);

### Yield - While

To package up a standard `do .. while` loop, for instance if your sequence terminates based on its own output, use the `Yield - While` pattern.

    var random = new Random();
    var numbers = Loop.Yield(() => random.NextDouble())
                        .While(n => n < 0.7);

    var list = new List<double>(numbers);
    Assert.IsFalse(list.Any(n => n >= 0.7));

## NotNull<T>

This is essentially the inverse of `Nullable<T>` - the idea is to encode in the type of a variable or the signature of a method the intent that a reference value should not be null. It's a typesafe way of expressing this idiom.

    void Something(Wotsit w)
    {
        if (w == null)
            throw new ArgumentNullException("w");

        // ...
    }

Like this.

    void Something(NotNull<Wotsit> w)
    {
        // ...
    }

It comes with implicit operators to allow for assignments like this

    var wotsit = new Wotsit();
    Something(wotsit);

and

    NotNull<Wotsit> wotsit = new Wotsit();
    Wotsit w = wotsit;

As well as a convenience factory function

    var name = Not.Null("jeff");

It overrides equality operators for easy comparison

    NotNull<string> name = "Bill";

    Assert.IsTrue("Ralph" != name);

The underlying value, if any, is also available via the `Value` property

    NotNull<Func<string>> getString = Not.Null<Func<string>>(() => "hi");

    Assert.AreEqual("hi", getString.Value());

Trying to construct a `NotNull<T>` with a `null` value results in a `NullValueException`, if this is bypassed via standard CLR struct initialisation (i.e. `new NotNull<string>()`) then the same exception is thrown on attempted access of the exception.

## Option<T>

This is a C# friendly implementation of the standard [Option type](https://en.wikipedia.org/wiki/Option_type) with some factory methods to aid type inference. It has a similar API to `Nullable<T>` with analogous `HasValue` and `Value` properties but, unlike `Nullable<T>`, is not restricted to value types. Similar to `Nullable<T>`, accessing the `Value` property of a 'None' value results in a `NoneAccessException`.

It's a value type so you always get a usable value (even if it's a None).

It also has a companion object, `Option` which is home to a set of extension methods that provide most of the functionality.

    var opt = Some.Of("banana");

    Assert.IsTrue(opt.HasValue);
    Assert.IsTrue(Option.HasValue(opt));
    Assert.AreEqual("banana", opt.Value);
    Assert.AreEqual("banana", Option.GetValue(opt));

    opt = None.Of<string>();

    Assert.IsFalse(opt.HasValue);
    Assert.IsFalse(Option.HasValue(opt));

    // opt.Value and Option.GetValue(opt) throw NoneAccessException

Here are some of the supported operations.

### Factories

    var o1 = Some.Of(new Person { Name = "Billy-Bob" });
    // equivalent to new Option<Person>(new Person { ...

    var o2 = None.Of<Person>();
    // equivalent to new Option<Person>();

> Note that `Some.Of` has no opinion of the value you pass it, `Some.Of<Person>(null)` yields an option with a value, just a null value. If you want to coerce values based on their 'nullness' use `FromRef` for reference types and `FromVal` for nullable value types.

So this code would result in a `NullReferenceException`

    Person person = null;
    var name Some.Of(person)
        .Map(p => p.Name);

### Join

Standard `join` operation.

    var opt = Some.Of(Some.Of("hi")).Join();

    Assert.AreEqual("hi", opt.Value);

### Map

Like `fmap` for functors.

    var o =
        Some.Of("12")
        .Map(Int32.Parse)
        .Map(i => i * i);

    Assert.AreEqual(144, o.Value);

### Bind

The monadic bind.

    Option<int> TryParse(string s)
    {
        int v;
        return Int32.TryParse(s, out v)
            ? Some.Of(v)
            : None.Of<int>();
    }

    void Demo()
    {
        var o = Some.Of("47")
            .Bind(TryParse);

        Assert.AreEqual(47, o.Value);
    }

### Try

For cases where you aren't bothered about catching an exception, you just want the success/failure.

    var o = Option.Try(() => Int32.Parse("123"));

    Assert.AreEqual(123, o.Value);

    o = Option.Try(() => Int32.Parse("banana"));

    Assert.IsFalse(o.HasValue);

### TryBind

For easily bringing the common `bool TryXXX(input, out output)` idiom into the `Option` world.

    var o = Some.Of("34")
        .TryBind<string,int>(Int32.TryParse);

    Assert.AreEqual(34, o.Value);

    o = Some.Of("hedgehog")
        .TryBind<string, bool>(Boolean.TryParse);

    Assert.IsFalse(o.HasValue);

    o = None.Of<string>()
        .TryBind<string, decimal>(Decimal.TryParse);

    Assert.IsFalse(o.HasValue);
 
### TryMap

When the function you're mapping might throw.

    var o = Some
        .Of("76")
        .TryMap(Int32.Parse);

    Assert.AreEqual(76, o.Value);

    var o = Some
        .Of("_")
        .TryMap(Int32.Parse);

    Assert.IsFalse(o.HasValue);

### FirstOption

Because `Enumerable.FirstOrDefault` is misleading for value types

    var u = new List<int> { 0 }.FirstOrDefault();
    var v = new List<int>().FirstOrDefault();

    Assert.AreEqual(0, u); 
    Assert.AreEqual(0, v); // really?

Whereas

    var o = new List<int>().FirstOption();

    Assert.IsFalse(o.HasValue);

and

    var o = new List<int> { 5, 4, 3, 2, 1 }.FirstOption();

    Assert.AreEqual(5, o.Value);

### Or

For getting a value, one way or another

    var o = Some.Of("zebra");

    Assert.AreEqual("zebra", o.Or("marmoset"));

    o = None.Of<string>();

    Assert.AreEqual("marmoset", o.Or("marmoset"));

### OrDefault

When that really is what you want.

    var d = None.Of<decimal>();

    Assert.AreEqual(0m, d.OrDefault());

    var s = None.Of<string>();

    Assert.AreEqual((string)null, s.OrDefault());

### FromRef

For coercing reference types to options

    string s = null;
    var o = Option.FromRef(s);

    Assert.IsFalse(o.HasValue);

    s = "hello";
    o = Option.FromRef(s);

    Assert.AreEqual("hello", o.Value);

### FromVal

For coercing nullable value types to options

    int? i = null;
    var o = Option.FromVal(i);

    Assert.IsFalse(o.HasValue);

    i = 57;
    o = Option.FromVal(i);

    Assert.AreEqual(57, o.Value);

### LINQ Support

The `Option<T>` type comes with some support for LINQ so you can write expressions like

    var o = from v in Some.Of("67")
            select Int32.Parse(v);

    Assert.AreEqual(67, o.Value);

    o = from v in None.Of<string>()
            select Int32.Parse(v);

    Assert.IsFalse(o.HasValue);

and

    var o = from a in Some.Of(12)
            from b in Some.Of(23)
            select a + b;

    Assert.AreEqual(35, o.Value);

    o = from a in Some.Of(12)
            from b in None.Of<int>()
            select a + b;

    Assert.IsFalse(o.HasValue);

    o = from a in None.Of<int>()
            from b in Some.Of(23)
            select a + b;

    Assert.IsFalse(o.HasValue);

and

    var o = from v in Some.Of(5)
            where v % 2 == 0
            select v;

    Assert.IsFalse(o.HasValue);

    o = from v in Some.Of(6)
            where v % 2 == 0
            select v;

    Assert.AreEqual(6, o.Value);

## Wrap

For wrapping logic up with other operations, perhaps for logging etc.

To be honest, this doesn't do much other than reify certain patterns of delegates

For void operations

    var output = new StringBuilder();
    Action<object> log = s => output.Append(s.ToString());

    var wrapped = Nifty.Wrap.Up(action =>
    {
        log(1);
        try
        {
            action();
        }
        catch (Exception)
        {
            log(3);
        }
        finally
        {
            log(4);
        }
    });

    wrapped.Do(() =>
    {
        log(2);
        int d = 0;
        log(2 / d);
    });

    Assert.AreEqual("1234", output.ToString());

and for functions that return values

    var output = new StringBuilder();
    var wrapped = Nifty.Wrap.Up<double>(f =>
        {
            try
            {
                return f();
            }
            catch (Exception e)
            {
                output.AppendLine(e.Message);
                return -1;
            }
        });

    var n = 1;
    var d = 0;
    var r = wrapped.Do(() => n / d);

    Assert.AreEqual(-1, r);
    Assert.IsFalse(String.IsNullOrEmpty(output.ToString()));
