﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nifty.Tests
{
    public class OptionTests
    {
        [Test]
        public void None_by_default()
        {
            var opt = new Option<int>();

            Assert.IsFalse(opt.HasValue);
            Assert.IsFalse(Option.HasValue(opt));
        }

        [Test]
        public void None_is_None()
        {
            var opt = None.Of<int>();

            Assert.IsFalse(opt.HasValue);
            Assert.IsFalse(Option.HasValue(opt));
        }

        [Test]
        [ExpectedException(typeof(NoneAccessException))]
        public void Cannot_get_value_from_None_instance()
        {
            var opt = None.Of<int>();

            var i = opt.Value;
        }

        [Test]
        [ExpectedException(typeof(NoneAccessException))]
        public void Cannot_get_value_from_None_static()
        {
            var opt = None.Of<string>();

            var s = Option.GetValue(opt);
        }

        [Test]
        public void Value_preserved_val()
        {
            var i = new Option<int>(42);

            Assert.AreEqual(42, i.Value);
            Assert.AreEqual(42, Option.GetValue(i));
        }

        [Test]
        public void Value_preserved_ref()
        {
            var s = new Option<string>("hi");

            Assert.AreEqual("hi", s.Value);
            Assert.AreEqual("hi", Option.GetValue(s));
        }

        [Test]
        public void Join_handles_None()
        {
            Option<int> o = None
                .Of<Option<int>>()
                .Join();

            Assert.IsFalse(o.HasValue);
        }

        [Test]
        public void Join_handles_Some_of_None()
        {
            Option<string> o = Some
                .Of(None.Of<string>())
                .Join();

            Assert.IsFalse(o.HasValue);
        }

        [Test]
        public void Join_handles_Some_of_Some()
        {
            Option<decimal> o = Some
                .Of(Some.Of(159m))
                .Join();

            Assert.AreEqual(159m, o.Value);
        }

        Option<int> TryParse(string s)
        {
            int v;
            return Int32.TryParse(s, out v)
                ? Some.Of(v)
                : None.Of<int>();
        }

        [Test]
        public void Bind_handles_Some()
        {
            var o = Some.Of("47")
                .Bind(TryParse);

            Assert.AreEqual(47, o.Value);
        }

        [Test]
        public void Bind_handles_None()
        {
            var o = None.Of<string>()
                .Bind(TryParse);

            Assert.IsFalse(o.HasValue);
        }

        [Test]
        public void TryBind_handles_Some_success()
        {
            var o = Some.Of("34")
                .TryBind<string,int>(Int32.TryParse);

            Assert.AreEqual(34, o.Value);
        }

        [Test]
        public void TryBind_handles_Some_failure()
        {
            var o = Some.Of("hedgehog")
                .TryBind<string, bool>(Boolean.TryParse);

            Assert.IsFalse(o.HasValue);
        }

        [Test]
        public void TryBind_handles_None()
        {
            var o = None.Of<string>()
                .TryBind<string, decimal>(Decimal.TryParse);

            Assert.IsFalse(o.HasValue);
        }

        [Test]
        public void Try_success_yields_value()
        {
            var o = Option.Try(() => Int32.Parse("123"));

            Assert.AreEqual(123, o.Value);
        }

        [Test]
        public void Try_failure_yields_None()
        {
            var o = Option.Try(() => Int32.Parse("banana"));

            Assert.IsFalse(o.HasValue);
        }

        [Test]
        public void Map_handles_value()
        {
            var o =
                Some.Of("12")
                .Map(Int32.Parse)
                .Map(i => i * i);

            Assert.AreEqual(144, o.Value);
        }

        [Test]
        public void Map_handles_None()
        {
            var o =
                None.Of<string>()
                .Map(Int32.Parse)
                .Map(i => i * i);

            Assert.IsFalse(o.HasValue);
        }

        [Test]
        public void TryMap_handles_success()
        {
            var o = Some
                .Of("76")
                .TryMap(Int32.Parse);

            Assert.AreEqual(76, o.Value);
        }

        [Test]
        public void TryMap_handles_failure()
        {
            var o = Some
                .Of("_")
                .TryMap(Int32.Parse);

            Assert.IsFalse(o.HasValue);
        }

        [Test]
        public void FirstOrDefault_yields_incorrect_result_from_empty_sequence_for_value_types()
        {
            var v = new List<int>().FirstOrDefault();

            Assert.AreEqual(0, v); // really?
        }

        [Test]
        public void FirstOption_yields_first_value_correctly()
        {
            var o = new List<int> { 5, 4, 3, 2, 1 }.FirstOption();

            Assert.AreEqual(5, o.Value);
        }

        [Test]
        public void FirstOption_yields_None_for_empty_sequence()
        {
            var o = new List<int>().FirstOption();

            Assert.IsFalse(o.HasValue);
        }

        [Test]
        public void Or_yields_value()
        {
            var o = Some.Of("zebra");

            Assert.AreEqual("zebra", o.Or("marmoset"));
        }

        [Test]
        public void Or_handles_None()
        {
            var o = None.Of<string>();

            Assert.AreEqual("marmoset", o.Or("marmoset"));
        }

        [Test]
        public void OrDefault_works_for_value_types()
        {
            var o = None.Of<decimal>();

            Assert.AreEqual(0m, o.OrDefault());
        }

        [Test]
        public void OrDefault_works_for_reference_types()
        {
            var o = None.Of<string>();

            Assert.AreEqual((string)null, o.OrDefault());
        }

        [Test]
        public void Ref_works_on_null()
        {
            string s = null;
            var o = Option.FromRef(s);

            Assert.IsFalse(o.HasValue);
        }

        [Test]
        public void Ref_works_on_value()
        {
            string s = "hello";
            var o = Option.FromRef(s);

            Assert.AreEqual("hello", o.Value);
        }

        [Test]
        public void Val_works_on_null()
        {
            int? i = null;
            var o = Option.FromVal(i);

            Assert.IsFalse(o.HasValue);
        }

        [Test]
        public void Val_works_on_value()
        {
            int? i = 57;
            var o = Option.FromVal(i);

            Assert.AreEqual(57, o.Value);
        }

        [Test]
        public void Linq_Select_Some()
        {
            var o = from v in Some.Of("67")
                    select Int32.Parse(v);

            Assert.AreEqual(67, o.Value);
        }

        [Test]
        public void Linq_Select_None()
        {
            var o = from v in None.Of<string>()
                    select Int32.Parse(v);

            Assert.IsFalse(o.HasValue);
        }

        [Test]
        public void Linq_SelectMany_Some_Some()
        {
            var o = from a in Some.Of(12)
                    from b in Some.Of(23)
                    select a + b;

            Assert.AreEqual(35, o.Value);
        }

        [Test]
        public void Linq_SelectMany_Some_None()
        {
            var o = from a in Some.Of(12)
                    from b in None.Of<int>()
                    select a + b;

            Assert.IsFalse(o.HasValue);
        }

        [Test]
        public void Linq_SelectMany_None_Some()
        {
            var o = from a in None.Of<int>()
                    from b in Some.Of(23)
                    select a + b;

            Assert.IsFalse(o.HasValue);
        }

        [Test]
        public void Linq_Where_false()
        {
            var o = from v in Some.Of(5)
                    where v % 2 == 0
                    select v;

            Assert.IsFalse(o.HasValue);
        }

        [Test]
        public void Linq_Where_true()
        {
            var o = from v in Some.Of(6)
                    where v % 2 == 0
                    select v;

            Assert.AreEqual(6, o.Value);
        }
    }
}
