﻿using Nifty.Loops;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nifty.Tests
{
    public class EnumerableWrapTests
    {
        [Test]
        [ExpectedException(typeof(NullValueException))]
        public void EnumerableWrap_rejects_null()
        {
            new EnumerableWrap<string>(null);
        }

        [Test]
        public void EnumerableWrap_correctly_wraps_enumerable()
        {
            var list = new List<int> { 1, 2, 3, 4, 5, 6, 7 };

            var wrap = new EnumerableWrap<int>(list);

            foreach (var i in wrap)
            {
                Assert.IsTrue(list.Contains(i));
            }

            Assert.AreEqual(list.Count, wrap.Count());
        }

        [Test]
        public void EnumerableWrap_works_as_non_generic_enumerable()
        {
            var list = new List<int> { 1, 2, 3, 4, 5, 6, 7 };

            var wrap = new EnumerableWrap<int>(list);

            System.Collections.IEnumerable oldEnum = wrap;

            var count = 0;
            foreach (object o in oldEnum)
            {
                Assert.IsTrue(list.Contains((int)o));
                ++count;
            }

            Assert.AreEqual(list.Count, count);
        }
    }
}
