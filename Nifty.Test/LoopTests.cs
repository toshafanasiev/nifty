﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nifty.Tests
{
    public class LoopTests
    {
        [Test]
        public void Loop_For_ref_alphabet()
        {
            var alphabet = Loop.For('a', c => c <= 'z', (ref char c) => c++);

            var list = alphabet.ToList();

            Assert.AreEqual(26, list.Count);
            var upperAlpha = new String(alphabet.Select(c => Char.ToUpper(c)).ToArray());
            Assert.AreEqual("ABCDEFGHIJKLMNOPQRSTUVWXYZ", upperAlpha);
        }

        [Test]
        public void Loop_For_alphabet()
        {
            var alphabet = Loop.For('a', c => c <= 'z', c => ++c);

            var list = alphabet.ToList();

            Assert.AreEqual(26, list.Count);
            var upperAlpha = new String(alphabet.Select(c => Char.ToUpper(c)).ToArray());
            Assert.AreEqual("ABCDEFGHIJKLMNOPQRSTUVWXYZ", upperAlpha);
        }

        [Test]
        public void Reverse_alphabet()
        {
            var reverse = Loop.For('z', c => c >= 'a', c => --c);

            Assert.AreEqual("zyxwvutsrqponmlkjihgfedcba", new String(reverse.ToArray()));
        }

        [Test]
        public void Loop_While_Yield_run_count()
        {
            var random = new Random();
            var numbers = Loop.While(run => run.Count < 10)
                              .Yield(run => random.NextDouble())
                              .ToList();

            Assert.AreEqual(numbers.Count, 10);
        }

        [Test]
        public void Loop_While_Yield_container_fill()
        {
            var list = new List<double>();
            var random = new Random();
            var numbers = Loop.While(run => list.Count < 10)
                              .Yield(run => random.NextDouble());
            
            list.AddRange(numbers);

            Assert.AreEqual(list.Count, 10);
        }

        [Test]
        public void Loop_While_Yield_run_count_as_output()
        {
            var list = new List<int>();
            var counts = Loop.While(run => run.Count < 10)
                             .Yield(run => run.Count);

            list.AddRange(counts);

            Assert.AreEqual(list.Count, 10);
            Assert.AreEqual(list[0], 0);
        }

        [Test]
        public void Loop_While_Do_fills_container()
        {
            var list = new List<int>();
            Loop.While(run => list.Count < 10)
                .Do(run => list.Add(run.Count));

            Assert.AreEqual(list.Count, 10);
        }

        [Test]
        public void Loop_Yield_While_random()
        {
            var random = new Random();
            var numbers = Loop.Yield(() => random.NextDouble())
                               .While(n => n < 0.7);

            var list = new List<double>(numbers);
            Assert.IsFalse(list.Any(n => n >= 0.7));
        }
    }
}
