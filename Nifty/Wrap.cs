﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nifty
{
    public sealed class Wrap
    {
        private readonly Action<Action> wrapper;

        private Wrap(NotNull<Action<Action>> wrapper)
        {
            this.wrapper = wrapper;
        }

        public static Wrap Up(Action<Action> wrapper)
        {
            return new Wrap(wrapper);
        }

        public static VWrap<T> Up<T>(Func<Func<T>, T> wrapper)
        {
            return new VWrap<T>(wrapper);
        }

        public void Do(Action action)
        {
            wrapper(action);
        }

        public sealed class VWrap<T>
        {
            private readonly Func<Func<T>, T> wrapper;

            internal VWrap(Func<Func<T>, T> wrapper)
            {
                this.wrapper = wrapper;
            }

            public T Do(Func<T> func)
            {
                var value = wrapper(func);

                return value;
            }
        }
    }
}
