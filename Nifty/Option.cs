﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nifty
{
    public struct Option<T>
    {
        private readonly bool hasValue;
        private readonly T value;

        public Option(T value)
        {
            this.hasValue = true;
            this.value = value;
        }

        public bool HasValue
        {
            get { return hasValue; }
        }

        public T Value
        {
            get
            {
                if (!hasValue)
                    throw new NoneAccessException();

                return value;
            }
        }
    }

    public static class Option
    {
        public delegate bool TryPattern<in I, R>(I input, out R result);

        public static bool HasValue<T>(this Option<T> opt)
        {
            return opt.HasValue;
        }

        public static T GetValue<T>(this Option<T> opt)
        {
            return opt.Value;
        }

        public static Option<A> Join<A>(this Option<Option<A>> opt)
        {
            return opt.Or(None.Of<A>());
        }

        public static Option<T> Try<T>(Func<T> f)
        {
            try
            {
                return Some.Of(f());
            }
            catch
            {
                return None.Of<T>();
            }
        }

        public static Option<B> Map<A, B>(this Option<A> opt, Func<A, B> f)
        {
            if (opt.HasValue)
                return Some.Of(f(opt.Value));
            else
                return None.Of<B>();
        }

        public static Option<B> Bind<A, B>(this Option<A> a, Func<A, Option<B>> f)
        {
            return a.Map(f).Join();
        }

        public static Option<B> TryBind<A, B>(this Option<A> a, TryPattern<A, B> f)
        {
            return a.Bind(i =>
            {
                B b;
                return f(i, out b)
                    ? Some.Of(b)
                    : None.Of<B>();
            });
        }

        public static Option<B> TryMap<A, B>(this Option<A> a, Func<A, B> f)
        {
            return Try(() => a.Map(f)).Join();
        }

        public static Option<T> FirstOption<T>(this IEnumerable<T> items)
        {
            using (var e = items.GetEnumerator())
                return e.MoveNext()
                    ? Some.Of(e.Current)
                    : None.Of<T>();
        }

        public static T Or<T>(this Option<T> opt, T value)
        {
            return opt.HasValue ? opt.Value : value;
        }

        public static T OrDefault<T>(this Option<T> opt)
        {
            return opt.Or(default(T));
        }

        public static Option<T> FromRef<T>(T value)
            where T : class
        {
            return value != null
                ? Some.Of(value)
                : None.Of<T>();
        }

        public static Option<T> FromVal<T>(T? value)
            where T : struct
        {
            return value.HasValue
                ? Some.Of(value.Value)
                : None.Of<T>();
        }
        
        public static Option<B> Select<A, B>(this Option<A> a, Func<A, B> f)
        {
            return a.Map(f);
        }

        public static Option<C> SelectMany<A, B, C>(this Option<A> a, Func<A, Option<B>> f, Func<A, B, C> p)
        {
            return a.Bind(va => f(va).Bind(vb => Some.Of(p(va, vb))));
        }

        public static Option<A> Where<A>(this Option<A> a, Func<A, bool> pred)
        {
            return a.Map(pred).Or(false) ? a : None.Of<A>();
        }
    }

    public static class Some
    {
        public static Option<T> Of<T>(T value)
        {
            return new Option<T>(value);
        }
    }

    public static class None
    {
        public static Option<T> Of<T>()
        {
            return new Option<T>();
        }
    }
}
