﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Nifty
{
    public static class Not
    {
        public static NotNull<T> Null<T>(T value)
            where T : class
        {
            return new NotNull<T>(value);
        }

        public static NotNull<Expression<T>> NullExpr<T>(Expression<T> expr)
            where T : class
        {
            return new NotNull<Expression<T>>(expr);
        }
    }

    public struct NotNull<T>
        where T : class
    {
        private readonly T value;

        internal NotNull(T value)
        {
            this.value = Check(value);
        }

        public static implicit operator NotNull<T>(T value)
        {
            return new NotNull<T>(value);
        }

        public static implicit operator T(NotNull<T> instance)
        {
            return instance.Value;
        }

        public static bool operator ==(NotNull<T> lhs, NotNull<T> rhs)
        {
            return lhs.Value == rhs.Value;
        }

        public static bool operator ==(T lhs, NotNull<T> rhs)
        {
            return lhs == rhs.Value;
        }

        public static bool operator ==(NotNull<T> lhs, T rhs)
        {
            return lhs.Value == rhs;
        }

        public static bool operator !=(NotNull<T> lhs, NotNull<T> rhs)
        {
            return lhs.Value != rhs.Value;
        }

        public static bool operator !=(T lhs, NotNull<T> rhs)
        {
            return lhs != rhs.Value;
        }

        public static bool operator !=(NotNull<T> lhs, T rhs)
        {
            return lhs.Value != rhs;
        }

        public override bool Equals(object obj)
        {
            return this.Value.Equals(obj);
        }

        public override int GetHashCode()
        {
            return this.Value.GetHashCode();
        }

        public T Value
        {
            get { return Check(value); }
        }

        private static T Check(T value)
        {
            if (value == null)
                throw new NullValueException();

            return value;
        }
    }
}
