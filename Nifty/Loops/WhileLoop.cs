﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nifty.Loops
{
    public class WhileLoop
    {
        private readonly Func<LoopRunContext, bool> test;

        internal WhileLoop(Func<LoopRunContext, bool> test)
        {
            this.test = test;
        }

        private IEnumerable<T> YieldImpl<T>(Func<LoopRunContext, T> generator)
        {
            var context = new RunContext();
            while (test(context))
            {
                yield return generator(context);
                context.count++;
            }
        }

        public EnumerableWrap<T> Yield<T>(Func<LoopRunContext, T> generator)
        {
            return new EnumerableWrap<T>(YieldImpl(generator));
        }

        public void Do(Action<LoopRunContext> action)
        {
            var context = new RunContext();
            while (test(context))
            {
                action(context);
                context.count++;
            }
        }

        class RunContext
            : LoopRunContext
        {
            public int count = 0;

            public override int Count
            {
                get { return count; }
            }
        }
    }
}
