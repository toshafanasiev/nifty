﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nifty.Loops
{
    public class EnumerableWrap<T>
        : IEnumerable<T>
    {
        private readonly NotNull<IEnumerable<T>> enumerable;

        public EnumerableWrap(IEnumerable<T> enumerable)
        {
            this.enumerable = Not.Null(enumerable);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return enumerable.Value.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return enumerable.Value.GetEnumerator();
        }
    }
}
