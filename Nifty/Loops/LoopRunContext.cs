﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nifty.Loops
{
    public abstract class LoopRunContext
    {
        public abstract int Count { get; }
    }
}
