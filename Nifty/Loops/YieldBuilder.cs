﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nifty.Loops
{
    public class YieldBuilder<T>
    {
        private readonly Func<T> generator;

        internal YieldBuilder(Func<T> generator)
        {
            this.generator = generator;
        }

        private IEnumerable<T> WhileImpl(Func<T, bool> test)
        {
            var value = generator();
            while (test(value))
            {
                yield return value;
                value = generator();
            }
        }

        public EnumerableWrap<T> While(Func<T, bool> test)
        {
            return new EnumerableWrap<T>(WhileImpl(test));
        }
    }
}
