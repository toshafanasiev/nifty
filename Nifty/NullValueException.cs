﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nifty
{
    public sealed class NullValueException
        : ArgumentNullException
    {
    }
}
